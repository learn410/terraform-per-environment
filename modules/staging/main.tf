terraform {
  required_version = ">= 1.1.9"
  backend "http" {
  }
}

module "terraform-per-environment" {
  source      = "../terraform-per-environment"
  environment = "staging"
}
