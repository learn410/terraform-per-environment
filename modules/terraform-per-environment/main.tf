terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.1.9"
}

provider "aws" {
  region = "eu-west-1"
}

module "vars" {
  source      = "../vars"
  environment = var.environment
}

resource "aws_cloudwatch_log_group" "test_log_group" {
  name = "/test/${module.vars.log_group_name}"

  retention_in_days = 1
}

variable "environment" {
  type = string
}
