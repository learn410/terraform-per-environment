terraform {
  required_version = ">= 1.1.9"
}

variable "environment" {
  type = string
}

variable "log_group_name" {
  type = map(any)
  default = {
    production = "test-log-group-production"
    staging    = "test-log-group-staging"
  }
}

output "log_group_name" {
  value = lookup(var.log_group_name, var.environment)
}
