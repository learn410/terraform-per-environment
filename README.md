This project began as learning how to create a Terraform configuration that
supports multiple environments. 

It is now for establishing best practices for Terraform and Gitlab CI in
general.

## Terraform

### Module Structure

Grouped into 2 + 1 per environment modules. Which are then composed together.

1. /modules/vars

This module contains the variables for the Project's infrastructure. For all
environmets. It outputs them to make them accessible to modules that include it
via composition.

2. /modules/[Project Name]

This module contains the Terraform config for the Project.

3. /modules/[Environment]

A module per environment. Here we specify which environment it is to Terraform,
then compose module 2 into this module.

### Publishing modules.

Modules are published to the GitLab Terraform registry in the CI, when you tag a
commit. The workflow is:

- branch of changes (trunk based dev)
- merge to main
- tag main locally, push up
- new pipeline will run on tag the publishes modules, one for each env.
